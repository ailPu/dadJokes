import { getRandomDadJoke } from "./utils/fetchDadJoke";
import { buttonData } from "./buttonData.js";

document.addEventListener("DOMContentLoaded", async () => {
	const randomIndex = () => {
		return Math.floor(Math.random() * buttonData.length);
	};
	let initialIndex = randomIndex();
	let initial = true;
	let prevIndex;
	const dadJokeText = document.querySelector("#dad-joke-text");
	const button = document.querySelector("#dynamic-joke");
	const buttonText = button.querySelector("#joke-button-text");
	const loader = document.querySelector(".lds-dual-ring");
	buttonText.innerHTML = buttonData[initialIndex];
	dadJokeText.innerText = await getRandomDadJoke();

	button.addEventListener("click", async () => {
		loader.classList.toggle("hidden");
		const joke = await getRandomDadJoke();
		let newIndex = randomIndex();
		if (initial === true) {
			while (initialIndex === newIndex) {
				newIndex = randomIndex();
			}
			initial = false;
		}
		while (prevIndex === newIndex) {
			newIndex = randomIndex();
		}
		prevIndex = newIndex;
		loader.classList.toggle("hidden");
		dadJokeText.innerText = joke;
		buttonText.innerHTML = buttonData[newIndex];
	});
});
